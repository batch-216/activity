package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

//mark this Java object as a representation of a database via @Entity.
@Entity
//designate table name
@Table(name = "posts")
public class Post {
//    indicate that this property represents the primary key
    @Id
//    values for this property will ve auto-incremented
    @GeneratedValue
    private Long id;
//    class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

//    empty constructor
    public Post(){}

//    parameterized constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

//    getters & setters

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(){
        this.content = content;
    }

}
